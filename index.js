const express = require('express')
const fs = require('fs')
const app = express()

//set port
const PORT = process.env.PORT || 5000;
app.use(express.static(__dirname + '/public'))

//set views
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

//set controllers
const homeController = require('./routers/home.js')
app.use('/', homeController)

//set listen
app.listen(PORT, () => {
    console.log('Node app is running on port', PORT)
})